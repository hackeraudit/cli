//! # `haudit` is a reference command-line client for Hacker Audit

extern crate app_dirs;
extern crate clap;
#[macro_use]
extern crate error_chain;
extern crate hackeraudit_api_common;
extern crate ignore;
extern crate pem;
extern crate reqwest;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate term;
extern crate toml;

use ignore::WalkBuilder;
use clap::{Arg, SubCommand};
use std::fmt;
use std::path::{Path, PathBuf};
use std::fs;
use std::io::Read;
use std::io::Write;
use serde::de::Error as DeError;
use std::collections::BTreeSet;
use hackeraudit_api_common::*;

mod errors {

    use super::*;

    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain! {
        errors {
            AuditNotPassed {
                description("some packages did not pass the audit")
                display("some pacages did not pass the audit")
            }
        }

        foreign_links {
            Deserializer(toml::de::Error);
            Serializer(toml::ser::Error);
            Io(std::io::Error);
            Json(serde_json::Error);
            Request(reqwest::Error);
            Pem(pem::errors::Error);
            AppDir(app_dirs::AppDirsError);
        }
    }
}

use errors::{Error, ErrorKind, Result, ResultExt};

#[derive(PartialOrd, PartialEq, Ord, Eq, Debug, Clone, Copy)]
enum RiskLevel {
    None,
    Low,
    Medium,
    High,
}

impl fmt::Display for RiskLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use RiskLevel::*;
        write!(
            f,
            "{}",
            match *self {
                None => "NON",
                Low => "LOW",
                Medium => "MED",
                High => "HIG",
            }
        )
    }
}

#[derive(PartialOrd, PartialEq, Ord, Eq, Clone)]
struct Risk {
    level: RiskLevel,
    reason: String,
}

impl Risk {
    fn calculate_for(pkg_r: &AuditResponsePackage) -> Self {
        if pkg_r.self_score_good {
            return Risk {
                level: RiskLevel::None,
                reason: "Positive review by self".into(),
            };
        }

        if pkg_r.self_score_bad {
            return Risk {
                level: RiskLevel::High,
                reason: "Negative review by self".into(),
            };
        }

        // TODO: Implement in a smarter way.
        // For now, just keep sorted in risk order
        if pkg_r.stats.minor.bad * 3 > pkg_r.stats.minor.good {
            return Risk {
                level: RiskLevel::High,
                reason: "Very bad reviews".into(),
            };
        }

        if pkg_r.stats.minor.bad * 6 > pkg_r.stats.minor.good {
            return Risk {
                level: RiskLevel::Medium,
                reason: "Bad bad reviews".into(),
            };
        }

        if pkg_r.stats.patch.used < 2 {
            return Risk {
                level: RiskLevel::Medium,
                reason: "Very few users".into(),
            };
        }

        if pkg_r.stats.minor.good < 1 {
            return Risk {
                level: RiskLevel::Medium,
                reason: "Few good reviews".into(),
            };
        }

        if pkg_r.stats.minor.bad * 9 > pkg_r.stats.minor.good {
            return Risk {
                level: RiskLevel::Low,
                reason: "Some bad reviews".into(),
            };
        }

        if pkg_r.stats.minor.used < 10 {
            return Risk {
                level: RiskLevel::Low,
                reason: "Few users".into(),
            };
        }

        return Risk {
            level: RiskLevel::None,
            reason: "Positive review by self".into(),
        };
    }
}

fn print_and_analyze_response(request: &AuditRequest, response: &AuditResponse) -> Result<()> {
    let mut fail = false;

    let mut pkg_by_risk: Vec<_> = response
        .packages
        .iter()
        .cloned()
        .map(|r| r.map(|r| (Risk::calculate_for(&r), r)))
        .enumerate()
        .collect();

    pkg_by_risk.sort_by(|a, b| match (&a.1, &b.1) {
        (&Err(_), &Err(_)) => std::cmp::Ordering::Equal,
        (&Err(_), &Ok(_)) => std::cmp::Ordering::Less,
        (&Ok(_), &Err(_)) => std::cmp::Ordering::Greater,
        (&Ok(ref a), &Ok(ref b)) => a.0
            .level
            .cmp(&b.0.level)
            .then_with(|| a.1.url.cmp(&b.1.url)),
    });

    for (i, res) in pkg_by_risk.into_iter() {
        let pkg = &request.packages[i];
        match res {
            Ok((risk, resp)) => {
                if risk.level >= RiskLevel::Low {
                    println!(
                        "{} {:32}{:16} {}",
                        risk.level,
                        pkg.name,
                        pkg.version,
                        resp.url
                            .as_ref()
                            .map(|s| s.as_str())
                            .unwrap_or_else(|| "Not found - retry later"),
                    );
                    fail = true;
                }
            }
            Err(ref e) => {
                println!("{} {:32}{:16} {}", "ERR", pkg.name, pkg.version, e);
            }
        }
    }
    if fail {
        bail!(ErrorKind::AuditNotPassed)
    }
    Ok(())
}

fn scan_lockfiles(settings: &Settings) -> Result<AuditRequest> {
    let mut packages = BTreeSet::new();
    for result in WalkBuilder::new("./").build() {
        match result {
            Err(e) => {
                eprintln!("WalkDir error: {}", e);
            }
            Ok(path) => match path.file_name().to_str() {
                Some("Cargo.toml") => {
                    let res =
                        handle_cargo_lock(path.path().with_file_name("Cargo.lock"), &mut packages);
                    if !settings.ignore_err {
                        res?
                    } else {
                        if res.is_err() {
                            eprintln!("Ignoring error for: {}", path.path().display());
                        }
                    }
                }
                _ => {}
            },
        }
    }

    Ok(AuditRequest {
        packages: packages.iter().cloned().collect(),
        token: None,
    })
}

fn set_token(token: &str, settings: &Settings) -> Result<()> {
    let config_path = config_path(settings)?;

    let config = if config_path.exists() {
        let mut config = read_config(&config_path)?;
        config.token = Some(token.into());
        config
    } else {
        std::fs::create_dir_all(config_path.parent().expect("path parent"))?;

        Config {
            token: Some(token.into()),
        }
    };

    write_config(&config_path, config)?;

    Ok(())
}

fn handle_cargo_lock(path: PathBuf, packages: &mut BTreeSet<AuditRequestPackage>) -> Result<()> {
    let mut content = String::new();
    fs::File::open(path)?.read_to_string(&mut content)?;

    let value = content.parse::<toml::Value>()?;
    for package in value
        .get("package")
        .ok_or(toml::de::Error::custom("package section not found"))?
        .as_array()
        .ok_or(toml::de::Error::custom("package section not found"))?
    {
        let name = package["name"]
            .as_str()
            .ok_or(toml::de::Error::custom("no name for package"))?;
        let version = package["version"]
            .as_str()
            .ok_or(toml::de::Error::custom("no version for package"))?;

        packages.insert(AuditRequestPackage {
            source: "crates.io".into(),
            name: name.into(),
            version: version.into(),
        });
    }
    Ok(())
}

fn open_pem(path: &Path) -> Result<Vec<u8>> {
    let mut s = String::new();
    fs::File::open(path)?.read_to_string(&mut s)?;

    Ok(pem::parse(&s)?.contents)
}

fn submit_audit_request(request: &mut AuditRequest, settings: &Settings) -> Result<AuditResponse> {
    let mut client = reqwest::Client::builder();

    request.token = settings.token.clone();

    for cert_path in &settings.additional_certs {
        let der = open_pem(&cert_path)
            .chain_err(|| format!("Couldn't add root certificate: {}", cert_path.display()))?;
        let cert = reqwest::Certificate::from_der(&der)?;
        client.add_root_certificate(cert);
    }

    let mut res = client
        .build()?
        .post(&format!("{}/api/0/audit", settings.haudit_host))
        .json(request)
        .send()?
        .error_for_status()?;

    Ok(res.json()?)
}

fn audit(settings: &mut Settings) -> Result<()> {
    read_config_into(settings)?;
    if settings.token.is_none() {
        eprintln!("Token not found. Your requests will be IP-based rate-limited, and some features won't work.");
        eprintln!();
        eprintln!("Check https://hackeraudit.com/token and call:");
        eprintln!();
        eprintln!("    {} token <TOKEN>", std::env::args().next().unwrap());
        eprintln!();
    }

    let mut request = scan_lockfiles(settings)?;
    let response = submit_audit_request(&mut request, &settings)?;
    print_and_analyze_response(&request, &response)?;
    Ok(())
}

#[derive(Default)]
struct Settings {
    config_path: Option<PathBuf>,
    additional_certs: Vec<PathBuf>,
    token: Option<String>,
    ignore_err: bool,
    haudit_host: String,
}

impl Settings {
    fn new() -> Self {
        let mut settings: Self = Default::default();
        settings.haudit_host = "https://hackeraudit.com".to_owned();
        settings
    }
}

#[derive(Deserialize, Serialize)]
struct Config {
    token: Option<String>,
}

fn config_path(settings: &Settings) -> Result<PathBuf> {
    let info = app_dirs::AppInfo {
        name: "hackeraudit",
        author: "hackeraudit.com",
    };

    Ok(if let Some(ref path) = settings.config_path {
        path.clone()
    } else {
        let config_dir = app_dirs::get_app_root(app_dirs::AppDataType::UserConfig, &info)?;

        config_dir.join("config.toml")
    })
}

fn read_config(path: &Path) -> Result<Config> {
    let mut file = std::fs::File::open(&path)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(toml::from_str(&s)?)
}

fn write_config(path: &Path, config: Config) -> Result<()> {
    let mut tmp_path = path.to_owned();
    tmp_path.set_extension("tmp");

    let data = toml::to_string(&config)?;
    let mut file = std::fs::File::create(&tmp_path)?;
    file.write_all(&data.as_bytes())?;
    fs::rename(tmp_path, path)?;
    Ok(())
}

fn read_config_into(settings: &mut Settings) -> Result<()> {
    let config_path = config_path(settings)?;

    if config_path.exists() {
        let config = read_config(&config_path)?;
        settings.token = config.token;
    }

    Ok(())
}

fn run() -> Result<()> {
    let mut settings = Settings::new();
    let matches = clap::App::new("haudit")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Dawid Ciężarkiewicz <dpc@dpc.pw>")
        .about("Hacker Audit CLI")
        .arg(
            Arg::with_name("CERT_PATH")
                .long("cert")
                .takes_value(true)
                .value_name("CERT_PATH")
                .help("Path to additional SSL Cert to use."),
        )
        .arg(
            Arg::with_name("HOST_URI")
                .long("host")
                .takes_value(true)
                .value_name("HOST_URI")
                .help("Hacker audit URI (default: https://hackeraudit.com)"),
        )
        .subcommand(
            SubCommand::with_name("audit")
                .display_order(0)
                .about("Audit current directory")
                .arg(
                    Arg::with_name("IGNORE_ERRROS")
                        .long("ignore-errors")
                        .help("Ignore most errors"),
                ),
        )
        .subcommand(
            SubCommand::with_name("token")
                .about("Set user token")
                .arg(Arg::with_name("TOKEN").required(true)),
        )
        .setting(clap::AppSettings::SubcommandRequiredElseHelp)
        .get_matches();

    if let Some(path) = matches.value_of_os("CERT_PATH") {
        settings.additional_certs.push(PathBuf::from(path))
    }

    if let Some(host) = matches.value_of_os("HOST_URI") {
        settings.haudit_host = String::from_utf8_lossy(host.to_str().unwrap().as_bytes()).into();
    }

    match matches.subcommand() {
        ("audit", Some(args)) => {
            if args.is_present("IGNORE_ERRROS") {
                settings.ignore_err = true;
            } else {
                settings.ignore_err = false;
            }
            audit(&mut settings)?
        }
        ("token", Some(args)) => if let Some(token) = args.value_of("TOKEN") {
            set_token(token, &settings)?
        },
        (cmd, _) => panic!("Unrecognized subcommand: {}", cmd),
    }

    Ok(())
}

fn main() {
    match run() {
        Err(Error(ErrorKind::AuditNotPassed, _)) => {
            ::std::process::exit(1);
        }
        Err(ref e) => {
            eprintln!("error: {}", e);

            for e in e.iter().skip(1) {
                eprintln!("caused by: {}", e);
            }

            // The backtrace is not always generated. Try to run this example
            // with `RUST_BACKTRACE=1`.
            if let Some(backtrace) = e.backtrace() {
                eprintln!("backtrace: {:?}", backtrace);
            }

            ::std::process::exit(2);
        }
        Ok(()) => {}
    }
}
